const express = require("express");
const challengeRoutes = require("./routes/challenge.routes");
const app = express();
const mongodb = require("./db/mongodb.connect");

// connect to db
mongodb.connect();

app.use(express.json());

app.use("/challenge", challengeRoutes);

//error handling middleware
app.use((resObj, req, res, next) => {
  res.status(400).json(resObj);
});

app.get("/", (req, res) => {
  res.json("Hello world!");
});

module.exports = app;
