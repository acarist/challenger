const express = require("express");
const { check } = require("express-validator");
const recordController = require("../controllers/record.controller");
const router = express.Router();

// request validation with express validator (validator.js)
const checkers = [
  check("minCount", "minCount should be a number")
    .not()
    .isEmpty()
    .toInt()
    .isInt(),
  check("maxCount", "maxCount should be a valid integer number")
    .not()
    .isEmpty()
    .toInt()
    .isInt(),
  check("startDate", "startDate must be YYYY-MM-DD format")
    .not()
    .isEmpty()
    .custom(isValidDate),
  check(
    "endDate",
    "endDate must be YYYY-MM-DD format and bigger than startDate"
  )
    .not()
    .isEmpty()
    .custom(isValidDate)
    .custom((value, { req }) => {
      // custom validator for date order is correct
      if (req.body.startDate && value) {
        const start_date = new Date(req.body.startDate);
        const end_date = new Date(value);
        return end_date >= start_date;
      } else {
        return true;
      }
    }),
];

// custom validator for date format
function isValidDate(value) {
  if (value) {
    if (!value.match(/^\d{4}-\d{2}-\d{2}$/)) return false;

    const date = new Date(value);
    if (!date.getTime()) return false;
    return date.toISOString().slice(0, 10) === value;
  } else {
    return true;
  }
}

// one and only route
router.post("/", checkers, recordController.getRecords);

module.exports = router;
