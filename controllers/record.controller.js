const { validationResult } = require("express-validator");
const RecordModel = require("../models/record.model");

exports.getRecords = async (req, res, next) => {
  let responseObject = {};
  responseObject.code = 0;
  responseObject.msg = "Success";
  responseObject.records = [];

  const validationErrors = validationResult(req);

  if (!validationErrors.isEmpty()) {
    responseObject.code = 1;

    responseObject.msg = validationErrors.array();
    next(responseObject);
  }

  try {
    const allRecords = await RecordModel.aggregate([
      {
        $project: {
          key: 1,
          createdAt: 1,
          totalCount: { $sum: "$counts" },
        },
      },
      {
        $match: {
          totalCount: { $gt: req.body.minCount, $lt: req.body.maxCount },
          createdAt: {
            $gte: new Date(req.body.startDate),
            $lt: new Date(req.body.endDate),
          },
        },
      },
    ]);
    responseObject.records = allRecords;
    res.status(200).json(responseObject);
  } catch (err) {
    responseObject.code = 1;
    responseObject.msg = err.message;
    next(responseObject);
  }
};
