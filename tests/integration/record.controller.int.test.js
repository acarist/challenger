const request = require("supertest");
const app = require("../../app");
const mockRequest = require("../mockdata/mockrequest.json");

const endpointUrl = "/challenge/";

describe(endpointUrl, () => {
  test("POST without params " + endpointUrl, async () => {
    const response = await request(app).post(endpointUrl).send();
    expect(response.statusCode).toBe(400);
    expect(response.body.code).toBe(1);
    expect(response.body.msg).not.toBe("Success");
  });
  test("POST with params " + endpointUrl, async () => {
    const response = await request(app).post(endpointUrl).send(mockRequest);
    expect(response.statusCode).toBe(200);
    expect(response.body.code).toBe(0);
    expect(response.body.msg).toBe("Success");
  });
});
