const RecordController = require("../../controllers/record.controller");
const RecordModel = require("../../models/record.model");
const httpMocks = require("node-mocks-http");
const mockRequest = require("../mockdata/mockrequest.json");
const mockRows = require("../mockdata/mockrows.json");
const mockResponse = require("../mockdata/mockresponse.json");

jest.mock("../../models/record.model");

let req, res, next;
beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = jest.fn();
});

describe("RecordController.getRecords", () => {
  it("should have a getRecords function", () => {
    expect(typeof RecordController.getRecords).toBe("function");
  });
  it("should call RecordModel.aggregate({})", async () => {
    req.params = mockRequest;
    await RecordController.getRecords(req, res, next);
    expect(RecordModel.aggregate).toHaveBeenCalledTimes(1);
  });
  it("should return json body and response code 200", async () => {
    req.params = mockRequest;
    RecordModel.aggregate.mockReturnValue(mockRows);
    await RecordController.getRecords(req, res, next);
    expect(res.statusCode).toBe(200);
    expect(res._getJSONData()).toStrictEqual(mockResponse);
    expect(res._isEndCalled()).toBeTruthy();
  });
  it("should handle errors in getRecords", async () => {
    const errorMessage = { message: "Error finding" };
    const rejectedPromise = Promise.reject(errorMessage);
    RecordModel.aggregate.mockReturnValue(rejectedPromise);
    await RecordController.getRecords(req, res, next);
    expect(next).toHaveBeenCalledWith({
      code: 1,
      msg: errorMessage.message,
      records: [],
    });
  });
  it("should call error middleware when error occur", async () => {
    const errorMessage = { message: "Error finding" };
    const rejectedPromise = Promise.reject(errorMessage);
    RecordModel.aggregate.mockReturnValue(rejectedPromise);
    await RecordController.getRecords(req, res, next);
    expect(next).toHaveBeenCalled();
  });
});
