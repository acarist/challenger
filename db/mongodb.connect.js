const mongoose = require("mongoose");

require('dotenv').config()

const dbString = process.env.MONGO_DB;

async function connect() {
  try {
    await mongoose.connect(dbString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  } catch (err) {
    console.error("Error connecting to mongodb");
    console.error(err);
  }
}

module.exports = { connect };
