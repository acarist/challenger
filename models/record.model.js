const mongoose = require("mongoose");

const RecordSchema = new mongoose.Schema({
  key: {
    type: String,
  },
  createdAt: {
    type: Date,
  },
  counts: [
    {
      type: Number,
    },
  ],
  value: {
    type: String,
  },
});

const RecordModel = mongoose.model("Record", RecordSchema);

module.exports = RecordModel;
