# Challenger

Challenger is a api that satisfy aggregated result using mongodb collection. It has one endpoint. Tested and developed with nodejs version 14.x . (you can see in dockerfile)

### Before Run
Rename ```.env-sample``` to ```.env``` end fill ```MONGO_DB``` information.

### Run Locally
Install npm packages
```
npm install
```

and run!
```
npm run start
```

### Run with Docker

```
docker-compose up
```

### Tests
Project test  files (unit, integration) located in ```tests``` folder.

Run tests with
```npm run test```

### Sample Requests
If everythings ok:
```
Request:
curl --location --request POST 'localhost:3000/challenge' \
--header 'Content-Type: application/json' \
--data-raw '{
"startDate": "2016-01-26",
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
}' 

Response:
{
    "code": 0,
    "msg": "Success",
    "records": [
        {
            "_id": "5ee21587e07f053f990ceb06",
            "key": "HmsYvNTB",
            "createdAt": "2016-06-12T21:50:44.088Z",
            "totalCount": 2917
        },
        {
            "_id": "5ee21587e07f053f990ceb07",
            "key": "jOjBYTLV",
            "createdAt": "2016-11-13T19:54:23.677Z",
            "totalCount": 2954
        }
        ...
    ]
}
```

there is errors:
```
Request (startDate missing):
curl --location --request POST 'localhost:3000/challenge' \
--header 'Content-Type: application/json' \
--data-raw '{
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
}' 

Response:
{
    "code": 1,
    "msg": [
        {
            "msg": "startDate must be YYYY-MM-DD format",
            "param": "startDate",
            "location": "body"
        }
    ],
    "records": []
}
```

### Heroku Test
heroku app adress is ```https://rocky-island-06084.herokuapp.com```

example request:
```
curl --location --request POST 'https://rocky-island-06084.herokuapp.com/challenge' \
--header 'Content-Type: application/json' \
--data-raw '{
"startDate": "2016-01-26",
"endDate": "2018-02-02",
"minCount": 2900,
"maxCount": 3000
}'
```